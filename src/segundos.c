#include <stdio.h>
#include "../include/utilfecha.h"

void parseSegToHoras(int seg){
	int  horas=0, minutos=0, segundos=0;
	horas=seg/HORA;
	minutos= (seg/MINUTO)%MINUTO;
	segundos=seg%MINUTO;

	printf("horas\tminutos\tsegundos\n");
	printf("%d\t%d\t%d\n",horas,minutos,segundos);
}